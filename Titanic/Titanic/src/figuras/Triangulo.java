/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package figuras;

import javax.media.opengl.GL;

/**
 *
 * @author Vale
 */
public class Triangulo {
    

    public Triangulo() {
    }
    
    public static void dibujarTriangulo(GL gl, float a, float b, float ancho,float alto){
            gl.glBegin(GL.GL_TRIANGLES);
            gl.glVertex3f(a + (ancho/2), b+alto, 0.0f);   // Top
            gl.glVertex3f(a, b, 0.0f); // Bottom Left
            gl.glVertex3f(a+ancho, b, 0.0f);  // Bottom Right
        gl.glEnd();
        
    }
    
    public static void dibujarTrianguloRectangulo(GL gl, float a, float b, float ancho,float alto){
            gl.glBegin(GL.GL_TRIANGLES);
            gl.glVertex3f(a , b+alto, 0.0f);   // Top
            gl.glVertex3f(a, b, 0.0f); // Bottom Left
            gl.glVertex3f(a+ancho, b, 0.0f);  // Bottom Right
        gl.glEnd();
        
    }
    
}
