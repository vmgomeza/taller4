/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package figuras;

import javax.media.opengl.GL;

/**
 *
 * @author Vale
 */
public class Circulo {
    
    
    
    public static void DibujarCirculo(GL gl,float a,float b,float radio){
        float x,y;
        gl.glBegin(gl.GL_POLYGON);
        for (int i = 0; i < 100; i++) {
            x=(float) (radio *Math.cos(i*2*Math.PI/100));
            y=(float) (radio *Math.sin(i*2*Math.PI/100));
            gl.glVertex2f(x+a,y+b);
            }
        gl.glEnd();
    }
    
    public static void DibujarSemicirculo(GL gl,float a,float b,float radio){
        float x,y;
         gl.glBegin(gl.GL_POLYGON);
        for (int i = 0; i < 51; i++) {
            x=(float) (radio *Math.cos(i*2*Math.PI/100));
            y=(float) (radio *Math.sin(i*2*Math.PI/100));
            gl.glVertex2f( x+a,y+b );
        }
        gl.glEnd();
    }
}
