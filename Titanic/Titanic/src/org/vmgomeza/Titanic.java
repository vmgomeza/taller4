package org.vmgomeza;


import com.sun.opengl.util.Animator;
import figuras.*;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;



/**
 * Titanic.java <BR>
 * author: Valeria G�mez. Alejandra Guaminga, Sandra Ipiales
 *
 * Este programa dibuja un barco que al chocar con un iceberg se parte en dos y se hunde
 */
public class Titanic implements GLEventListener {
    
   public static Frame frame = new Frame("Titanic");
   
//   float translax = -6;
//   float ang = 0;
//   float translay =0.15f;
   
    public static void main(String[] args) {
        
        GLCanvas canvas = new GLCanvas();
       
        canvas.addGLEventListener(new Titanic());
        frame.add(canvas);
        frame.setSize(840, 680);
        final Animator animator = new Animator(canvas);
        frame.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                // Run this on another thread than the AWT event queue to
                // make sure the call to Animator.stop() completes before
                // exiting
                new Thread(new Runnable() {

                    public void run() {
                        animator.stop();
                        System.exit(0);
                    }
                }).start();
            }
        });
        // Center frame
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        animator.start();
    }

    public void init(GLAutoDrawable drawable) {
        // Use debug pipeline
        // drawable.setGL(new DebugGL(drawable.getGL()));

        GL gl = drawable.getGL();
        System.err.println("INIT GL IS: " + gl.getClass().getName());

        // Enable VSync
        gl.setSwapInterval(1);

        // Setup the drawing area and shading mode
        gl.glClearColor(0.5412f, 0.7735f, 0.82f, 0.0f); //anaranjado paste
        gl.glShadeModel(GL.GL_SMOOTH); // try setting this to GL_FLAT and see what happens.
    }

    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        GL gl = drawable.getGL();
        GLU glu = new GLU();

        if (height <= 0) { // avoid a divide by zero error!
        
            height = 1;
        }
        final float h = (float) width / (float) height;
        gl.glViewport(0, 0, width, height);
        gl.glMatrixMode(GL.GL_PROJECTION);
        gl.glLoadIdentity();
        glu.gluPerspective(45.0f, h, 1.0, 20.0);
        gl.glMatrixMode(GL.GL_MODELVIEW);
        gl.glLoadIdentity();
    }

    public void display(GLAutoDrawable drawable) {
        
        GL gl = drawable.getGL();
        Fondo fondo = new Fondo();
        Barco barco = new Barco();
        Iceberg iceberg = new Iceberg();
        // Clear the drawing area
        gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
        // Reset the current matrix to the "identity"
        gl.glLoadIdentity();
        float x=frame.getWidth();
        float y=frame.getHeight();
        // Move the "drawing cursor" around
         gl.glTranslated(0.0f, 0.0f, -18.0f);
         fondo.DibujarFondo(gl, x, y);
         iceberg.DibujarIceberg(gl);
        // barco.DibujarBarco(gl);
         
         gl.glPushMatrix();
         gl.glTranslatef(Giro.translax, 0, 0);
         gl.glTranslatef(0, Giro.translay, 0);
         gl.glRotatef(Giro.angulo, 0,0, 1);  
         gl.glScalef(0.65f, 0.65f, 0.65f);
         barco.Barcoparteizquierda(gl);
         Giro.Giro();
         gl.glPopMatrix();

         gl.glPushMatrix();
         gl.glTranslatef(Giro.translax, 0, 0);
         gl.glTranslatef(0, Giro.translay, 0);
         gl.glRotatef(Giro.ang, 0,0,1);
         gl.glScalef(0.65f, 0.65f, 0.65f);
         barco.Barcopartederecha(gl);
         Giro.GiroInverso();
         gl.glPopMatrix();
         
//         //Dibujamos Mar
                  
         gl.glColor3f(0.3478f, 0.6419f, 0.74f); //color azul
         Cuadrado.dibujarCuadrado(gl, -y, -x,2*y, x-1.15f);
          
         
//         

            
            
        
        
         
//        //Dibujar cuadrados saltanto espacio
//            float a = -1.0f, b =0.0f,m = 0;
//            for (float i = a; i < 5 ; i++) {
//            m+=0.2;
//            Cuadrado.dibujarCuadrado(gl,i+m, b,1f, 1f);
//        }
        
//        Eje de coordenadas
//        gl.glColor3f(0.0f,0.0f,0.0f);
//        gl.glBegin(GL.GL_LINES);
//        
//        gl.glVertex2f(0, y);
//        gl.glVertex2f(0, -y);
        
//        gl.glVertex2f(-x, 0);
//        gl.glVertex2f(x, 0);
        

    
        
        // Flush all drawing operations to the graphics card
        gl.glFlush();
        
         
    }

    public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged) {
    }
}

