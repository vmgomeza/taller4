/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.vmgomeza;
import figuras.*;
import javax.media.opengl.GL;

public class Fondo {

    
    public void DibujarFondo(GL gl,float x,float y){
         
         //Dibujamos Mar
          gl.glColor3f(0.3478f, 0.6419f, 0.74f); //color azul
         Cuadrado.dibujarCuadrado(gl, -y, -x,2*y, x-0.5f);
         
         //Dibujamos Sol
         gl.glColor3f(0.96f, 0.8234f, 0.3744f); //amarillopastel
         Circulo.DibujarSemicirculo(gl, 0,-0.5f, 3);
        
    }
    
}
