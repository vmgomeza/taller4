/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.vmgomeza;

import javax.media.opengl.GL;
import figuras.*;
/**
 *
 * @author Vale
 */
public class Iceberg {
    
    public void DibujarIceberg(GL gl){
        
        gl.glTranslatef(1.5f, 0, 0);
       
        //Triangulo3
        gl.glScalef(2.0f, 2.0f, 2.0f);
        gl.glColor3f(0.51f, 0.9918f, 1.0f); //Celeste pastel
        Triangulo.dibujarTriangulo(gl, 1.5f, -0.5f, 2, 1.5f);
//        
        //Triangulos Intermedios
        gl.glColor3f(0.3038f, 0.9687f, 0.98f); //Celeste claro
        Triangulo.dibujarTriangulo(gl, 1.0f, -0.5f, 2f, 1.6f);
        //Triangulo central
        //gl.glColor3f(0.3362f, 0.7716f, 0.82f); //Celeste oscuro
        gl.glColor3f(0.3038f, 0.9687f, 0.98f); //Celeste claro
        Triangulo.dibujarTriangulo(gl, 1.6f, -0.55f, 2, 1.2f);
        gl.glColor3f(0.51f, 0.9918f, 1.0f); //Celeste pastel
        Triangulo.dibujarTriangulo(gl, 1.0f, -0.55f, 2, 1.2f);
 
         //TriangulosPequeños
        gl.glPushMatrix();
        gl.glColor4f(0.3038f, 0.9687f, 0.98f,0.1f); //Celeste claro
         gl.glScalef(0.5f, 0.5f, 0.5f);
         gl.glRotatef(0, 1,0, 1);
          Triangulo.dibujarTriangulo(gl, 3.4f, -1.2f, 2, 1.5f);
        gl.glPopMatrix();
        
        gl.glColor3f(0.51f, 0.9918f, 1.0f); //Celeste pastel
        Triangulo.dibujarTriangulo(gl, 0.3f, -0.6f, 1.8f, 1.4f);

        
//        //Sombra
//         gl.glPushMatrix();
//        gl.glColor3f(0.3362f, 0.7716f, 0.82f); //Celeste oscuro
//        gl.glEnable(GL.GL_BLEND);
//         gl.glTranslatef(2.85f, -1.25f, 0f);
//         gl.glScalef(0.5f, 0.5f, 0.5f);
//         gl.glRotatef(180, 0,0, 1);
//         Triangulo.dibujarTriangulo(gl, 1.4f, -1.2f, 2, 0.7f);
//         Triangulo.dibujarTriangulo(gl, 2.4f, -1.2f, 2, 0.7f);
//        gl.glPopMatrix();
//        
        
        
        //TriangulosPequeños
        gl.glPushMatrix();
         //gl.glTranslatef(1f, -0.25f, 0f);
         gl.glScalef(0.5f, 0.5f, 0.5f);
         gl.glRotatef(0, 1,0, 1);
         gl.glColor3f(0.51f, 0.9918f, 1.0f); //Celeste pastel
         Triangulo.dibujarTriangulo(gl, 5.3f, -1.2f, 2.2f, 2.2f);
        gl.glPopMatrix();
        
        //TriangulosPequeños
        gl.glPushMatrix();
        gl.glColor4f(0.3038f, 0.9687f, 0.98f,0.1f); //Celeste claro
         //gl.glTranslatef(1f, -0.25f, 0f);
         gl.glScalef(0.5f, 0.5f, 0.5f);
         gl.glRotatef(0, 1,0, 1);
         Triangulo.dibujarTriangulo(gl, 0.4f, -1.2f, 2, 1.5f);
          Triangulo.dibujarTriangulo(gl, 5.8f, -1.2f, 2, 1.5f);
         Triangulo.dibujarTriangulo(gl, 1.9f, -1.2f, 2.2f, 2.2f);
          Triangulo.dibujarTriangulo(gl, 5.1f, -1.2f, 1.8f, 0.8f);
        gl.glPopMatrix();
//  
////        gl.glPushMatrix();
////        gl.glRotatef(270, 0, 0, 1);//Giro espejo
////        Triangulo.dibujarTriangulo(gl, 1, 1, 2, 2);
////        gl.glPopMatrix();
    }
    
}
