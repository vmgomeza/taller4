package org.aguaminga;

import com.sun.opengl.util.Animator;
import com.sun.opengl.util.GLUT;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;
import javax.media.opengl.glu.GLUquadric;
import static java.lang.Math.random;


/**
 * SolarSystem.java Taller 4
 * @author Alejandra Guaminga
 * @version 2020-08-05
 */
public class SolarSystem implements GLEventListener {
   
    
    float ang =0, ang2=0, ang3=0, vx=-2.5f, vy=-2.5f;
    
    
    public static void main(String[] args) {
        Frame frame = new Frame("Sistema Solar");
        GLCanvas canvas = new GLCanvas();

        canvas.addGLEventListener(new SolarSystem());
        frame.add(canvas);
        frame.setSize(1080, 700);
        final Animator animator = new Animator(canvas);
        frame.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                // Run this on another thread than the AWT event queue to
                // make sure the call to Animator.stop() completes before
                // exiting
                new Thread(new Runnable() {

                    public void run() {
                        animator.stop();
                        System.exit(0);
                    }
                }).start();
            }
        });
        // Center frame
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        animator.start();
    }

    public void init(GLAutoDrawable drawable) {
        // Use debug pipeline
        // drawable.setGL(new DebugGL(drawable.getGL()));

        GL gl = drawable.getGL();
        System.err.println("INIT GL IS: " + gl.getClass().getName());

        // Enable VSync
        gl.setSwapInterval(1);

        // Setup the drawing area and shading mode
        gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f); //negro
        gl.glShadeModel(GL.GL_SMOOTH); 
    }

    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        GL gl = drawable.getGL();
        GLU glu = new GLU();

        if (height <= 0) { // avoid a divide by zero error!
        
            height = 1;
        }
        final float h = (float) width / (float) height;
        gl.glViewport(0, 0, width, height);
        gl.glMatrixMode(GL.GL_PROJECTION);
        gl.glLoadIdentity();
        glu.gluPerspective(70.0f, h, 1.0, 20.0);
        gl.glMatrixMode(GL.GL_MODELVIEW);
        gl.glLoadIdentity();
    }

    @Override
    public void display(GLAutoDrawable drawable) {
        GL gl = drawable.getGL();
        GLU glu= new GLU();
        GLUT glut = new GLUT();
        // Clear the drawing area
        gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
        // Reset the current matrix to the "identity"
        gl.glLoadIdentity();

        // Move the "drawing cursor" around
        gl.glTranslatef(-3.5f, 0.0f, -6.0f);
       
         
       gl.glPushMatrix();
       //Dibujar �rbitas
       Orbita orbmer=new Orbita(gl, 0.05f, -3.5f, 0.0f, 1.0f);
       orbmer.dibujaOrbita();
       Orbita orven=new Orbita(gl, 0.05f, -3.5f, 0.0f, 1.25f);
       orven.dibujaOrbita();
       Orbita ortie=new Orbita(gl, 0.05f, -3.5f, 0.0f, 1.5f);
       ortie.dibujaOrbita();
       Orbita orbmar=new Orbita(gl, 0.05f, -3.5f, 0.0f, 2.0f);
       orbmar.dibujaOrbita();
       Orbita orbjup=new Orbita(gl, 0.05f, -3.5f, 0.0f, 2.5f);
       orbjup.dibujaOrbita();
       Orbita orbsat=new Orbita(gl, 0.05f, -3.5f, 0.0f, 3.0f);
       orbsat.dibujaOrbita();
       Orbita orbur=new Orbita(gl, 0.05f, -3.5f, 0.0f, 3.5f);
       orbur.dibujaOrbita();
       Orbita orbnep=new Orbita(gl, 0.05f, -3.5f, 0.0f, 4.0f);
       orbnep.dibujaOrbita();
       
       gl.glPopMatrix();
        
       //      Dibujar el sol y los planetas
        gl.glPushMatrix();
        gl.glRotatef(ang, 1, 0, 0);

        Esfera sol=new Esfera(0.5f, 3.5f,0.0f,0.0f);
        sol.dibujaSol(gl, glut);
        gl.glPopMatrix();
        
        //Mercurio
        gl.glPushMatrix();
        
        //Para la translaci�n en x calculamos su componente en x (con seno) y para y igual
        //pero con coseno, los dos con el ang2 que es el va cambiando, en x le resto -1 para que
        //quede en la �rbita
        gl.glTranslated((-1 +(float) (Math.sin(ang2))),((float) (Math.cos(ang2))), 0);
        gl.glRotatef(ang, 1, 0, 0);
        Esfera mercurio=new Esfera(0.05f, 4.5f,0.0f,0.0f);
        mercurio.dibujaPlaneta(gl, glut, 0.69f, 0.9535f, 1.0f);//celeste
        gl.glPopMatrix();
        
        //Venus
        gl.glPushMatrix();
        
        //Para la translaci�n es lo mismo solo q voy aumentando 0.5f para ubicarle en la �rbita
        //a cada componente le multiplico x 1,25 para q se agrande la translacion
        //1,25 (en este caso) es el valor de "es" que se pone cuando se dibujan las �rbitas 
        //utilizo ang3 para que vaya en el otro sentido
        gl.glTranslated(-1.5 +(1.25 *(float) (Math.sin(ang3))),( 1.25*(float) (Math.cos(ang3))), 0);
        gl.glRotatef(ang, -1, 0, 0);
        Esfera venus=new Esfera(0.07f, 5f, 0.0f,0.0f);
        venus.dibujaPlaneta(gl, glut, 1.0f, 0.64f, 0.85f);//rosa
        gl.glPopMatrix();
        
        //Tierra
        gl.glPushMatrix();
        gl.glTranslated(-2 +(1.5 *(float) (Math.sin(ang2))),( 1.5*(float) (Math.cos(ang2))), 0);
        gl.glRotatef(ang, 1, 0, 0);
        //Le cambi� px para irle aumentando en 0.5, porq a lo q se movia se pasaba a la otra 
        //�rbita, si le quitas la translaci�n ves q esta dibujado en la orbita de arriba
        Esfera tierra=new Esfera(0.08f, 5.5f, 0.0f,0.0f);
        tierra.dibujaPlaneta(gl, glut, 0.1023f, 0.4334f, 0.93f);//azul
        gl.glPopMatrix();

        //Marte
         gl.glPushMatrix();
        gl.glTranslated(-2.5 +(2 *(float) (Math.sin(ang3))),( 2*(float) (Math.cos(ang3))), 0);
        gl.glRotatef(ang, 1, 0, 0);
        Esfera marte=new Esfera(0.09f, 6f, 0.0f,0.0f);
        marte.dibujaPlaneta(gl,glut, 0.96f, 0.096f, 0.096f); //rojo
         gl.glPopMatrix();
         
         //Jupiter
         gl.glPushMatrix();
        gl.glTranslated(-3 +(2.5 *(float) (Math.sin(ang2))),( 2.5*(float) (Math.cos(ang2))), 0);
        gl.glRotatef(ang, 1, 0, 0);
        Esfera jupiter=new Esfera(0.23f, 6.5f, 0.0f,0.0f);
        jupiter.dibujaPlaneta(gl,glut, 0.6674f, 0.1869f, 0.89f); //morado
         gl.glPopMatrix();
         
         //Saturno
         gl.glPushMatrix();
        gl.glTranslated(-3.5 +(3 *(float) (Math.sin(ang3))),( 3*(float) (Math.cos(ang3))), 0);
        gl.glRotatef(ang, 1, 0, 0);
         Esfera saturno=new Esfera(0.11f, 7f, 0.0f,0.0f);
        saturno.dibujaPlaneta(gl,  glut, 0.96f, 0.6374f, 0.3552f); //naranja
         gl.glPopMatrix();
         
         //Urano
         gl.glPushMatrix();
        gl.glTranslated(-4 +(3.5 *(float) (Math.sin(ang2))),( 3.5*(float) (Math.cos(ang2))), 0);
        gl.glRotatef(ang, 1, 0, 0);
        Esfera urano=new Esfera(0.08f, 7.5f, 0.0f,0.0f);
        urano.dibujaPlaneta(gl, glut, 0.829f, 0.96f, 0.3552f); //verde claro
         gl.glPopMatrix();
         
         //Neptuno
         gl.glPushMatrix();
        gl.glTranslated(-4.5 +(4 *(float) (Math.sin(ang3))),( 4*(float) (Math.cos(ang3))), 0);
        gl.glRotatef(ang, 1, 0, 0);
        Esfera neptuno=new Esfera(0.085f, 8f, 0.0f,0.0f);
        neptuno.dibujaPlaneta(gl, glut, 0.5467f, 0.7365f, 0.77f); //menta oscuro
         gl.glPopMatrix();
         //cambio en los �ngulos
        ang+=2f;
        ang2+=0.015f;
        ang3 -= 0.025f;
       
        //dibujar estrella fugaz
        gl.glPushMatrix();
        
        gl.glColor3f(1.0f, 1.0f, 1.0f); 
        gl.glTranslatef(this.vx,this.vy, 0f);
        gl.glScalef(0.08f, 0.08f, 0f);
        gl.glRotatef(-ang, 0, 1f, 0);
        glut.glutSolidTetrahedron();
        gl.glPopMatrix();
        vx+=0.00028f;
        vy+=0.00025f;
        
      
        gl.glPushMatrix();
        
        gl.glColor3f(1.0f, 1.0f, 1.0f); 
        gl.glTranslatef(this.vx+0.04f,this.vy+0.04f, 0f);
        gl.glScalef(0.1f, 0.1f, 0f);
        glut.glutSolidTetrahedron();
        gl.glPopMatrix();
        vx+=0.00028f;
        vy+=0.00025f;
        
        gl.glPushMatrix();
        
        gl.glColor3f(1.0f, 1.0f, 1.0f); 
        gl.glTranslatef(this.vx-0.04f,this.vy-0.04f, 0f);
        gl.glScalef(0.08f, 0.08f, 0f);
        gl.glRotatef(-ang, 0, 1f, 0);
        glut.glutSolidTetrahedron();
        gl.glPopMatrix();
        vx+=0.00028f;
        vy+=0.00025f;
       
        gl.glPushMatrix();
        
        gl.glPushMatrix();
        
        gl.glColor3f(1.0f, 1.0f, 1.0f); 
        gl.glTranslatef(this.vx-0.08f,this.vy-0.08f, 0f);
        gl.glScalef(0.08f, 0.08f, 0f);
        gl.glRotatef(-ang, 0, 1f, 0);
        glut.glutSolidTetrahedron();
        gl.glPopMatrix();
        vx+=0.00028f;
        vy+=0.00025f;
       
        gl.glPushMatrix();
        
        gl.glPushMatrix();
        
        gl.glColor3f(1.0f, 1.0f, 1.0f); 
        gl.glTranslatef(this.vx-0.12f,this.vy-0.12f, 0f);
        gl.glScalef(0.08f, 0.08f, 0f);
        gl.glRotatef(-ang, 0, 1f, 0);
        glut.glutSolidTetrahedron();
        gl.glPopMatrix();
        vx+=0.00028f;
        vy+=0.00025f;
       
        gl.glPushMatrix();
        // Flush all drawing operations to the graphics card
        gl.glFlush();
    }

    @Override
    public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged) {
    }
}

