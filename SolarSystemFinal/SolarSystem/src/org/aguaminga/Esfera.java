package org.aguaminga;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import com.sun.opengl.util.GLUT;
import javax.media.opengl.glu.GLU;
import javax.media.opengl.glu.GLUquadric;
/**
 * Taller 4
 * @author Alejandra Guaminga
 */
public class Esfera {
    
     public float posx, posy, posz;
    public float radio;
    public GL gl;
    
    public Esfera(float rad,  float px, float py, float pz){
        this.radio=rad;
        posx=px;
        posy=py;
        posz=pz;
    }
    
    public void dibujaSol(GL gl, GLUT glut){
        
         gl.glPushMatrix();
            gl.glColor3f(1.0f, 0.8405f, 0.13f); 
            gl.glTranslatef(posx,posy, posz);
            glut.glutSolidSphere(radio, 25, 25); 
            gl.glPopMatrix();
                      
    }
    
    
    public void dibujaPlaneta(GL gl, GLUT glut,float r, float g, float b){
                
         gl.glPushMatrix();
           gl.glColor3f(r, g, b); 
           gl.glTranslatef(posx,posy, posz);
           glut.glutSolidSphere(radio, 25, 25);
            
           gl.glPopMatrix();
              
           
           
    }
}


































