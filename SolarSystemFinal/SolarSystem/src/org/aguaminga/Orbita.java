package org.aguaminga;

import static java.lang.Math.cos;
import static java.lang.Math.sin;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
/**
 *
 * @author Alejandra Guaminga
 */
public class Orbita {
    GL gl;
    float rorb;
    float x, y;
    float es;
        
    public Orbita(GL gl, float rorb, float x, float y, float es) {
        this.gl = gl;
        this.rorb = rorb;
        this.x = x;
        this.y = y;
        this.es = es;
    }
 public void dibujaOrbita() {
        gl.glPushMatrix();
        gl.glColor3f(1.0f, 0.968f, 0.89f);
        gl.glBegin(gl.GL_LINES);
        
        for (float i = 0; i < 20; i+=0.05f) {
            float x = (float) Math.cos(i * 2 * Math.PI / 20);
            float y = (float) Math.sin(i * 2 * Math.PI / 20);
            gl.glVertex2f(this.es * x - this.x, this.es * y - this.y);
        }
        gl.glEnd();
        gl.glPopMatrix();
    }
    
}














